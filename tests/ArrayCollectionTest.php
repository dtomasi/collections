<?php
/**
 * ArrayCollection - OOP-Wrapper for Arrays
 *
 * @author Dominik Tomasi <dominik.tomasi@gmail.com>
 * @copyright tomasiMEDIA 2014
 *
 */

namespace Dtomasi\Tests\Containers;

use Dtomasi\Collections\ArrayCollection;

/**
 * Class ArrayCollectionTest
 * @package Dtomasi\Tests\Containers
 */
class ArrayCollectionTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var ArrayCollection
     */
    public $obj;

    public function setUp()
    {
        $this->obj = new ArrayCollection();
    }

    public function testSetAndGet()
    {
        $this->obj->set('foo', 'bar');
        $this->assertEquals('bar', $this->obj->get('foo'));
    }

    public function testAddAndGetValue()
    {
        $this->obj->add('foo');
        $this->assertEquals('foo', $this->obj->get($this->obj->search('foo')));
    }

    public function testFirstAndLast()
    {

        $this->obj->add('first');
        for ($i = 0; $i < 20; $i++) {
            $this->obj->add('foo');
        }
        $this->obj->add('last');

        $this->assertEquals('first', $this->obj->first());
        $this->assertEquals('last', $this->obj->last());
    }

    public function testNextAndKeyAndCurrent()
    {
        for ($i = 0; $i < 20; $i++) {
            $this->obj->add('foo-' . $i);
        }

        $this->assertEquals('foo-0', $this->obj->current());
        $this->assertEquals('foo-1', $this->obj->next());
        $this->assertEquals(1, $this->obj->key());
    }

    public function testToArray()
    {

        for ($i = 0; $i < 20; $i++) {
            $this->obj->add('foo-' . $i);
        }

        $this->assertTrue(is_array($this->obj->toArray()));

    }

    public function testClearAndIsEmpty()
    {

        for ($i = 0; $i < 20; $i++) {
            $this->obj->add('foo-' . $i);
        }

        $this->assertFalse($this->obj->isEmpty());

        $this->obj->clear();

        $this->assertTrue($this->obj->isEmpty());
    }

    public function testCount()
    {

        for ($i = 0; $i < 20; $i++) {
            $this->obj->add('foo-' . $i);
        }

        $this->assertEquals(20, $this->obj->count());
    }

    public function testRemove()
    {

        for ($i = 0; $i < 20; $i++) {
            $this->obj->set('foo-' . $i, 'bar-' . $i);
        }
        $this->assertEquals('bar-5', $this->obj->remove('foo-5'));
        $this->assertFalse($this->obj->has('foo-5'));
    }

    public function testRemoveElement()
    {

        for ($i = 0; $i < 20; $i++) {
            $this->obj->set('foo-' . $i, 'bar-' . $i);
        }

        $this->assertEquals('bar-5', $this->obj->removeElement('bar-5'));
        $this->assertFalse($this->obj->hasElement('bar-5'));

    }

    public function testSerializeAndUnserialize()
    {

        for ($i = 0; $i < 20; $i++) {
            $this->obj->set('foo-' . $i, 'bar-' . $i);
        }
        $originalArray = $this->obj->toArray();

        $strSerial = $this->obj->serialize();

        $this->assertTrue(is_string($strSerial));

        $this->obj->unserialize($strSerial);

        $rebuildArray = $this->obj->toArray();

        $this->assertTrue(count(array_diff($originalArray, $rebuildArray)) == 0);
    }

    public function testSetKeyAndNumericValues()
    {
        for ($i = 0; $i < 20; $i++) {
            $this->obj->set('foo-' . $i, 'bar-' . $i);
        }
        for ($i = 0; $i < 20; $i++) {
            $this->obj->add('bar-' . $i * 10);
        }

        $this->assertArrayHasKey('foo-0', $this->obj->toArray());
        $this->assertArrayHasKey(0, $this->obj->toArray());

        $this->assertEquals('bar-5', $this->obj->get('foo-5'));
        $this->assertEquals('bar-50', $this->obj->get(5));
    }

    public function testMultiDimArrays()
    {

        for ($i = 0; $i < 20; $i++) {
            $arrChild = array();
            for ($j = 0; $j < 20; $j++) {
                $arrChild['bar-' . $j * 10] = 'foo';
            }
            $this->obj->set('foo-' . $i, $arrChild);
        }

        $this->assertTrue(is_array($this->obj->get('foo-5')));
        $this->assertInstanceOf('Dtomasi\Collections\ArrayCollection', $this->obj->getArrayValueAsNewCollection('foo-5'));
    }

    public function testMergeCollections()
    {

        for ($i = 0; $i < 20; $i++) {
            $this->obj->set('origin-' . $i, 'foo-' . $i);
        }

        $mergeColl = new ArrayCollection();

        for ($i = 0; $i < 20; $i++) {
            $mergeColl->set('merged-' . $i, 'bar-' . $i);
        }

        $mergeColl->set('origin-3', 'bar-3');

        $this->obj->merge($mergeColl);

        $this->assertTrue($this->obj->has('merged-5'));
        $this->assertTrue(is_array($this->obj->get('origin-3')));
    }

    public function testReplace()
    {

        for ($i = 0; $i < 20; $i++) {
            $this->obj->set('bar-' . $i, 'foo-' . $i);
        }

        $replaceColl = new ArrayCollection();

        for ($i = 0; $i < 20; $i++) {
            $replaceColl->set('bar-' . $i, 'bar-' . $i);
        }

        $this->obj->replace($replaceColl);

        $this->assertEquals('bar-5', $this->obj->get('bar-5'));
    }

    public function testReverse()
    {

        for ($i = 0; $i < 20; $i++) {
            $this->obj->set('bar-' . $i, 'foo-' . $i);
        }

        $this->obj->reverse();

        $this->assertEquals('foo-19', $this->obj->first());
    }

    public function testDiff()
    {

        for ($i = 0; $i < 20; $i++) {
            $this->obj->set('bar-' . $i, 'foo-' . $i);
        }

        $diffColl = new ArrayCollection();

        for ($i = 0; $i < 10; $i++) {
            $diffColl->set('bar-' . $i, 'foo-' . $i);
        }

        $arrDiff = $this->obj->diff($diffColl);

        $this->assertEquals(10, count($arrDiff));
    }

}
