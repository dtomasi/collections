<?php

/**
 * Collection
 *
 * @author Dominik Tomasi <dominik.tomasi@gmail.com>
 * @copyright tomasiMEDIA 2014
 *
 */

namespace Dtomasi\Collections;

/**
 * Interface Collection
 * @package Dtomasi\Collections
 */

interface Collection extends \Countable, \Serializable, \Iterator
{

    /**
     * get the first element
     * @return mixed
     */
    public function first();

    /**
     * get the last element
     * @return mixed
     */
    public function last();

    /**
     * clear all data
     * @return void
     */
    public function clear();

    /**
     * set a element by key
     * @param $key string
     * @param $value mixed
     * @return void
     */
    public function set($key, $value);

    /**
     * add a value without a key
     * @param $value mixed
     * @return void
     */
    public function add($value);

    /**
     * Collection has a key ?
     * @param $key string
     * @return bool
     */
    public function has($key);

    /**
     * Collection has a value ?
     * @param $value mixed
     * @return bool
     */
    public function hasElement($value);

    /**
     * get value at key
     * @param $key string
     * @return mixed|null
     */
    public function get($key);

    /**
     * search by value
     * @param $value mixed
     * @return int|bool
     */
    public function search($value);

    /**
     * remove a element by key
     * @param $key string
     * @return mixed|null
     */
    public function remove($key);

    /**
     * remove a element by value
     * @param $value mixed
     * @return mixed|null
     */
    public function removeElement($value);

    /**
     * get all data as array
     * @return array
     */
    public function toArray();

    /**
     * is empty ?
     * @return bool
     */
    public function isEmpty();
} 