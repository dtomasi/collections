<?php
/**
 * ArrayCollection - OOP-Wrapper for Arrays
 *
 * @author Dominik Tomasi <dominik.tomasi@gmail.com>
 * @copyright tomasiMEDIA 2014
 *
 */

namespace Dtomasi\Collections;

/**
 * Class ArrayCollection
 * @package Dtomasi\Collections
 */
class ArrayCollection implements Collection
{
    /**
     * The data-array
     * @var array
     */
    protected $array;

    /**
     * Init the Collection with an array
     *
     * @param array $array
     */
    public function __construct(array $array = array())
    {
        $this->array = $array;
    }

    /**
     * {@inheritdoc}
     */
    public function rewind()
    {
        $this->first();
    }

    /**
     * {@inheritdoc}
     */
    public function first()
    {
        return reset($this->array);
    }

    /**
     * {@inheritdoc}
     */
    public function last()
    {
        return end($this->array);
    }

    /**
     * {@inheritdoc}
     */
    public function next()
    {
        return next($this->array);
    }

    /**
     * {@inheritdoc}
     */
    public function key()
    {
        return key($this->array);
    }

    /**
     * {@inheritdoc}
     */
    public function current()
    {
        return current($this->array);
    }

    /**
     * {@inheritdoc}
     */
    public function clear()
    {
        $this->array = array();
    }

    /**
     * {@inheritdoc}
     */
    public function valid()
    {
        return (current($this->array) == false ? false : true);
    }

    /**
     * {@inheritdoc}
     */
    public function set($key, $value)
    {
        $this->array[$key] = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function add($value)
    {
        $this->array[] = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function has($key)
    {
        return array_key_exists($key, $this->array);
    }

    /**
     * {@inheritdoc}
     */
    public function hasElement($value)
    {
        return (array_search($value, $this->array, true) == false ? false : true);
    }

    /**
     * {@inheritdoc}
     */
    public function get($key)
    {
        return ($this->has($key) ? $this->array[$key] : null);
    }

    /**
     * Get a returning Array as a new Collection
     * @param $key
     * @return ArrayCollection|null
     */
    public function getArrayValueAsNewCollection($key)
    {

        $value = ($this->has($key) ? $this->array[$key] : null);

        if (is_array($value)) {
            return new ArrayCollection($value);
        }
        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function search($value)
    {
        return array_search($value, $this->array, true);
    }

    /**
     * {@inheritdoc}
     */
    public function remove($key)
    {
        if ($this->has($key)) {
            $removed = $this->get($key);
            unset($this->array[$key]);
            return $removed;
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function removeElement($value)
    {
        if ($this->hasElement($value)) {
            $key = $this->search($value);
            $removed = $this->get($key);
            unset($this->array[$key]);
            return $removed;
        }
        return null;
    }

    /**
     * Alias for Search
     *
     * @param $value
     * @return bool|int
     */
    public function indexOf($value)
    {
        return $this->search($value);
    }

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return $this->array;
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return count($this->array);
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize($this->array);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($value)
    {
        $array = @unserialize($value);

        if (is_array($array)) {
            $this->array = $array;
        } else {
            throw new \ErrorException('value could not be converted to array');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty()
    {
        return !$this->array;
    }

    /**
     * @see array_reverse()
     */
    public function reverse()
    {
        $this->array = array_reverse($this->array);
    }

    /**
     * @see array_diff()
     *
     * @param ArrayCollection $collection
     * @return array
     */
    public function diff(ArrayCollection $collection)
    {
        return array_diff_assoc($this->array, $collection->toArray());
    }

    /**
     * Merge two Collections
     *
     * @param ArrayCollection $collection
     */
    public function merge(ArrayCollection $collection)
    {
        $this->array = array_merge_recursive($this->array, $collection->toArray());
    }

    /**
     * Replace values in Collection
     *
     * @param ArrayCollection $collection
     */
    public function replace(ArrayCollection $collection)
    {
        $this->array = array_replace_recursive($this->array, $collection->toArray());
    }

    /**
     * @see array_map()
     *
     * @param $callback
     * @return array
     */
    public function map($callback)
    {
        return array_map($callback, $this->array);
    }
} 
